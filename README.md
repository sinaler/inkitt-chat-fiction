### Inkitt Just another Hybrid Chat Fiction App

#### Dependencies

```
React
React Native (Expo)
React Native Web
Typescript
```

#### Demo

https://sinaler.github.io/inkitt-chat-fiction

#### Installation

Please run
```
yarn
```

#### Package Scripts

```
yarn start      // Run Metro Bundler
yarn web        // Run for web
yarn android    // Run for android
yarn ios        // Run for ios
yarn build      // Build for web
yarn deploy     // Deploy on Github Pages
yarn eslint     // Eslint check
yarn tsc        // Typescript check

```
