import {ViewStyle, TextStyle, Dimensions} from 'react-native';
const {width} = Dimensions.get('window');

const styles = {
  app: {
    height: '100%',
    padding: 25,
    backgroundColor: '#f1f1f1',
    overflow: 'hidden',
    justifyContent: 'space-between',
    alignSelf: 'stretch',
    width: width > 420 ? 420 : '100%',
    marginLeft: 'auto',
    marginRight: 'auto',
  } as ViewStyle,
  messages: {
    flexWrap: 'wrap',
    justifyContent: 'center',
    width: '100%',
    paddingBottom: 40,
    alignSelf: 'stretch',
  } as ViewStyle,
  message: {
    backgroundColor: '#dedee2',
    padding: 10,
    borderRadius: 5,
    marginBottom: 10,
  } as ViewStyle,
  messageRight: {
    backgroundColor: '#4e7ff6',
  } as ViewStyle,
  messageText: {
    color: 'black',
  } as TextStyle,
  messageName: {
    color: 'black',
    fontWeight: 'bold',
    marginBottom: 5,
  } as TextStyle,
  messageRightText: {
    color: 'white',
  } as TextStyle,
  action: {
    alignSelf: 'center',
    borderBottomColor: 'black',
    borderBottomWidth: 2,
    paddingBottom: 20,
  } as ViewStyle,
};

export default styles;
