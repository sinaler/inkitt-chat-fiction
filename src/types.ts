export type MessageType = {
  name: string;
  text: string;
  align: string;
}