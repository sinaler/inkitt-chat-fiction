import React, { FC, useState } from 'react';
import {
  View,
  Text,
  TouchableWithoutFeedback,
} from 'react-native';
import { FadeInView } from './components';
import { MessageType } from './types';
import messagesData from '../data/messages';

import styles from './styles';

const App: FC = () => {
  const [messages] = useState<MessageType[]>(messagesData);
  const [limit, setLimit] = useState<number>(1);
  const [isButtonEnabled, setButtonEnabled] = useState<boolean>(true);

  const showMessages = (limit: number): void => {
    setLimit(limit);
    setButtonEnabled(false);

    setTimeout(() => {
      setButtonEnabled(true);
    }, 1000);
  };

  const filteredMessages = messages.slice(0, limit);

  return (
    <View style={styles.app}>
      <TouchableWithoutFeedback
        onPress={(): void => setLimit (limit === 1 ? 1 : limit -1)}
      >
        <View style={styles.messages}>
          {filteredMessages.map(message =>
            <View
              key={message.text}
              style={[{
                flexDirection: 'row',
                alignItems: 'flex-end',
                justifyContent: message.align === 'right' ? 'flex-end' : 'flex-start'
              }]}
            >
              <View
                style={[styles.message,
                  message.align === 'right' && styles.messageRight]}
              >
              <FadeInView>
                <Text style={[styles.messageName, message.align === 'right' && styles.messageRightText]}>{message.name}</Text>
                <Text style={[styles.messageText, message.align === 'right' && styles.messageRightText]}>{message.text}</Text>
              </FadeInView>
            </View>
            </View>
          )}
        </View>
      </TouchableWithoutFeedback>
      {isButtonEnabled && limit < messages.length && <View>
        <TouchableWithoutFeedback
          onPress={(): void => showMessages(limit + 1)}
        >
          <View style={styles.action}>
            <Text>Tap to Continue</Text>
          </View>
        </TouchableWithoutFeedback>
      </View>}
    </View>
  )
};

export default App;
