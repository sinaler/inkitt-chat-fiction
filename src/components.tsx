import React, {useState, useEffect, FC} from 'react';
import { Animated } from 'react-native';

type Props = {
  children?: React.ReactNode;
};

export const FadeInView: FC = (props: Props) => {
  const [fadeAnim] = useState(new Animated.Value(0));

  useEffect(() => {
    Animated.timing(
      fadeAnim,
      {
        toValue: 1,
        duration: 500,
      }
    ).start();
  }, []);

  return (
    <Animated.View style={{opacity: fadeAnim}}>
      {props.children}
    </Animated.View>
  );
};