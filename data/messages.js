export default [
  {
    name: 'Summer',
    text: 'elliot is going to kill me',
    align: 'right',
  },
  {
    name: 'Scorp',
    text: 'busy, talk later?',
    align: 'left',
  },
  {
    name: 'Summer',
    text: 'i am serious, scorp',
    align: 'right',
  },
  {
    name: 'Summer',
    text: 'can you please help me?',
    align: 'right',
  },
  {
    name: 'Scorp',
    text: 'what are you saying?',
    align: 'left',
  },
  {
    name: 'Summer',
    text: 'i am saying, i haven\'t been honest to you',
    align: 'right',
  }
];